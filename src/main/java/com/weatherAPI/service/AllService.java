package com.weatherAPI.service;

import com.weatherAPI.demo.WeatherDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AllService {

    @Autowired
    WeatherDetails weatherDetails;

    public int day(List day) {
        for (int j = 0; j < day.size() - 1; j++) {
            double dayAvg = (double) day.get(j) + (double) day.get(j + 1);
            weatherDetails.setDayTemprature(dayAvg / day.size());
        }
        return 0;
    }

    public void night(List night) {
        for (int j = 0; j < night.size() - 1; j++) {
            double nightAvg = (double) night.get(j) + (double) night.get(j + 1);
            weatherDetails.setNightTemprature(nightAvg / night.size());
        }

    }

    public void pressure(List pressure) {
        for (int j = 0; j < pressure.size() - 1; j++) {
            int pressureAvg = (int) pressure.get(0) + (int) pressure.get(1);
            weatherDetails.setPressure(pressureAvg / pressure.size());
        }
    }
}

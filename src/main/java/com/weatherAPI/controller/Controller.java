package com.weatherAPI.controller;

import com.weatherAPI.demo.WeatherDetails;
import com.weatherAPI.repo.WeatherRepo;
import com.weatherAPI.service.AllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class Controller {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    WeatherDetails weatherDetails;

    @Autowired
    WeatherRepo repo;

    @Autowired
    AllService service;

    @PostMapping("/add/{cityName}")
    public List<WeatherDetails> getCity(@PathVariable String cityName) {
        weatherDetails.setCityName(cityName);

        HashMap<String, List> weatherData = this.restTemplate.getForObject("https://api.openweathermap.org/data/2.5/weather?q=" + cityName + "&appid=879167234a13175104e582db663c380d", HashMap.class);

        HashMap<String, List> mainValue = (HashMap<String, List>) weatherData.get("coord");
        List id = new ArrayList();
        id.add(weatherData.get("id"));
        int idSet = (int) id.get(0);

        weatherDetails.setId(idSet);

        HashMap<String, List> forecast = this.restTemplate.getForObject("https://api.openweathermap.org/data/2.5//onecall?lat=" + mainValue.get("lat") + "&" + "lon=" + mainValue.get("lon") + "&exclude=minutely,hourly,current&appid=879167234a13175104e582db663c380d", HashMap.class);

        List dailyValue = forecast.get("daily");

        List day = new ArrayList();
        List pressure = new ArrayList();
        List night = new ArrayList();

        for (int i = 0; i < 2; i++) {
            HashMap<String, List> dailyValueStore = (HashMap<String, List>) dailyValue.get(i);
            pressure.add(dailyValueStore.get("pressure"));
            Map<String,List> dayValue = (Map<String,List>) dailyValueStore.get("temp");
            day.add(dayValue.get("day"));
            night.add(dayValue.get("night"));
        }
        service.day(day);
        service.night(night);
        service.pressure(pressure);

        repo.save(weatherDetails);
        return repo.findAll();
    }

    @GetMapping("/data/{cityName}")
    public List<WeatherDetails> getData(@PathVariable String cityName) {
        return repo.findByCityName(cityName);
    }

}
package com.weatherAPI.demo;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class WeatherDetails {

    @Id
    private int id;
    private String cityName;
    private Double dayTemprature;
    private Double nightTemprature;
    private int pressure;

    public WeatherDetails(int id, String cityName, double dayTemprature, double nightTemprature, int pressure) {
        setId(id);
        setCityName(cityName);
        setDayTemprature(dayTemprature);
        setNightTemprature(nightTemprature);
        setPressure(pressure);
    }

    public WeatherDetails() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Double getDayTemprature() {
        return dayTemprature;
    }

    public void setDayTemprature(Double dayTemprature) {
        this.dayTemprature = dayTemprature;
    }

    public Double getNightTemprature() {
        return nightTemprature;
    }

    public void setNightTemprature(Double nightTemprature) {
        this.nightTemprature = nightTemprature;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    @Override
    public String toString() {
        return "WeatherDetails{" +
                "id=" + id +
                ", cityName='" + cityName + '\'' +
                ", dayTemprature=" + dayTemprature +
                ", nightTemprature=" + nightTemprature +
                ", pressure=" + pressure +
                '}';
    }
}

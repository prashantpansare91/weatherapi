package com.weatherAPI.repo;

import com.weatherAPI.demo.WeatherDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WeatherRepo extends JpaRepository<WeatherDetails, Integer> {

    public static final String name = "prashant";
     List<WeatherDetails> findByCityName(String cityName);
}

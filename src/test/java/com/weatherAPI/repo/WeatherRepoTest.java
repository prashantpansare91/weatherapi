package com.weatherAPI.repo;

import com.weatherAPI.demo.WeatherDetails;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


@SpringBootTest
class WeatherRepoTest {

    @Mock
    private WeatherRepo repo;

    @Test
    void test_findByCityName() {
        WeatherDetails weatherDetails = new WeatherDetails(1212, "Mumbai", 200.30, 230.30, 900);

        repo.save(weatherDetails);
        String city = "Mumbai";
        assertThat(repo.findAll()).isEqualTo(repo.findByCityName(city));

    }

}